#!/bin/bash

set -e

rm -rf */

if [ "0" -ne "`find */ -maxdepth 0 -type d 2>/dev/null | wc -l`" ]; then
  echo -e "\033[0;31mERROR: wrong dir count!\033[0m"
  exit 42
fi

read -s -p 'Bitbucket push Password:' passBB
echo

for repo in \
  'MSP430.js' \
  'picsim.js' \
  'ESJava' \
  'mazko.github.io' \
  'jsjavaparser' \
  'jssnowball' \
  'jsli' \
  'Manchester-Code' \
  'jstaggregator' \
  'bc.js'; do
  rm -rf "${repo}.git"
  git clone --bare "https://github.com/mazko/${repo}.git" || { echo 'ERROR clonning !!!' ; exit 1; }
  cd "${repo}.git" && \
  git push --mirror "https://mazko:${passBB}@gitlab.com/work-mirror-1/${repo}.git" && \
  cd - || { echo 'ERROR pushing !!!' ; exit 2; }
  sleep 5
done


for repo in \
  'ngspice.js' \
  'sed' \
  'awk' \
  'grep' ; do
  rm -rf "${repo}.git"
  git clone --bare "https://github.com/42ua/${repo}.git" || { echo 'ERROR clonning !!!' ; exit 1; }
  cd "${repo}.git" && \
  git push --mirror "https://mazko:${passBB}@gitlab.com/work-mirror-1/${repo}.git" && \
  cd - || { echo 'ERROR pushing !!!' ; exit 2; }
  sleep 5
done

for repo in \
  'tflite_kws' \
  'dataset' \
  'c_keyword_spotting' \
  'esp32_kws' ; do
  rm -rf "${repo}.git"
  git clone --bare "https://github.com/42io/${repo}.git" || { echo 'ERROR clonning !!!' ; exit 1; }
  cd "${repo}.git" && \
  git push --mirror "https://mazko:${passBB}@gitlab.com/work-mirror-1/${repo}.git" && \
  cd - || { echo 'ERROR pushing !!!' ; exit 2; }
  sleep 5
done

if [ "18" -ne "`find */ -maxdepth 0 -type d | wc -l`" ]; then
  echo -e '\033[0;31mERROR: wrong dir count!\033[0m'
  exit 24
fi
